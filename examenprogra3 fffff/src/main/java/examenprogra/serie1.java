package examenprogra;

import com.mycompany.examenprogra3.examen.Producto;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 *
 * @author solor
 */
public class serie1 {

    public static void main(String[] args) {
       List<Producto> productos = new ArrayList<>();
        productos.add(new Producto("vehiculo_simple",300));
        productos.add(new Producto("vehiculo_simple_auto",300));
        productos.add(new Producto("vehiculo_doble_traccion",200));
        productos.add(new Producto("vehiculo_alta_gama",800));
        productos.add(new Producto("motocicleta",230));


        Observable<Integer> productoObservableSuma =
                Observable
                        .from(productos)
                        .map(Producto::getPrecio)
                        .reduce(
                            new Func2<Integer, Integer, Integer>() {
                                @Override
                                public Integer call(Integer acumulador, Integer actual) {
                                    return acumulador + actual;
                                }
                            }
                        );

        Observable<Integer> productoObservableMayor =
                Observable
                        .from(productos)
                        .map(Producto::getPrecio)
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer mayor, Integer actual) {
                                        if (actual > mayor){
                                            mayor = actual;
                                        }
                                        return mayor;
                                    }
                                }
                        );

        Observable<Integer> productoObservableSumaConA =
                Observable
                        .from(productos).filter(
                                new Func1<Producto, Boolean>() {
                                    @Override
                                    public Boolean call(Producto t) {
                                        return t.getProducto().contains("a");
                                    }

                                }
                        )
                        .map(Producto::getPrecio)
                        .reduce(
                            new Func2<Integer, Integer, Integer>() {
                                @Override
                                public Integer call(Integer acumulador, Integer actual) {
                                    return acumulador + actual;
                                }
                            }
                        );


        productoObservableSuma.subscribe((sumatoria)->{
            System.out.println(" Sumatoria: " + sumatoria);
        });

        productoObservableMayor.subscribe((mayorValor)->{
            System.out.println(" El costo mayor es: " + mayorValor);
        });

        productoObservableSumaConA.subscribe((sumaConA)->{
            System.out.println(" La suma de productos con A es: " + sumaConA);
        });
    }
}
