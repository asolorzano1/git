package examenprogra;

import rx.Observable;
import rx.functions.Func1;
import rx.observables.MathObservable;

/**
 *
 * @author solor
 */
public class serie2 {
    
    public static void main(String[] args) {
        Integer[] numeros = {2,5,6,8,10,35,2,10};

        Observable numerosObservable = Observable.from(numeros);

        MathObservable.averageInteger(numerosObservable).subscribe((promedio)->{
            System.out.println("Promedio: " + promedio);
        });

        Observable numerosObservableMayor = Observable
                .from(numeros)
                .filter(
                new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(Integer t) {
                        return t >= 10;
                    }
                }
                );
       /*         .reduce(
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer acumulador, Integer actual) {
                                return acumulador + actual;
                            }
                        }
                );*/

        numerosObservableMayor.subscribe((filtroMayor) ->{
            System.out.println("Numeros mayores o igual que diez: " + filtroMayor);
        });


        MathObservable.sumInteger(numerosObservable).subscribe((suma) -> {
           System.out.println("Suma: " + suma);
        });
    }
}
